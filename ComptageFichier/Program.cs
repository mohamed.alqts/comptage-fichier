﻿/*
 * Auteur : Mohamed Al-Qutaish
 * Date : 21.10.22
 * Version : 2.0
 * Projet : ADNComptage
 * Description : Faire un comptage des lines d'ADN (ATGC) avec un boucle,if, etc...
 */
using System.IO;

String line;

StreamReader sr = new StreamReader("C:\\chromosome-11-partial.txt");
//affectation du variable line
line = sr.ReadLine();

int lineCount;
int A = 0;
int C = 0;
int G = 0;
int T = 0;

do
{
    lineCount = line.Length;
    // affichage du 1er caractère de la ligne
    for (int i = 0; i < lineCount; i++)//boucle qui lit toutes les caractères dans une ligne
    {
        if (line[i] == 'A')//ça veut dire qu'on incremente la variable de +1 si il y a la lettre qui est situé dans la condition
        {
            A = A + 1;
        }
        else
         if (line[i] == 'C')
        {
            C = C + 1;
        }
        else
         if (line[i] == 'G')
        {
            G = G + 1;
        }
        else
         if (line[i] == 'T')
        {
            T = T + 1;
        }
    }
    line = sr.ReadLine();
}
while (line != null);//boucle qui relie tout les lines si ils sont remplies

void ligne()
{
    Console.WriteLine("----------------------");
}

Console.WriteLine("Il y a " + A + " de A (premier caractère de la ligne)");
ligne();
Console.WriteLine("Il y a " + C + " de C (premier caractère de la ligne)");
ligne();
Console.WriteLine("Il y a " + G + " de G (premier caractère de la ligne)");
ligne();
Console.WriteLine("Il y a " + T + " de T (premier caractère de la ligne)");
ligne();

sr.Close();